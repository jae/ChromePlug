
/**
 * Created by SublimeText2.
 * User:JAE
 * Date: 2014-5-24
 * Blog:http://blog.jaekj.com
 * Email:hj.q@qq.com
 */
chrome.browserAction.onClicked.addListener(openURL);
function  openURL() {
	var bookmarkTreeNodes = chrome.bookmarks.getTree(
	  function(bookmarkTreeNodes) {
	      var root = bookmarkTreeNodes[0];
	      var urls = getBranch(root);
	      var rurl = urls[Random(1,urls.length)-1];
	      window.open(rurl);
	  });
}
function getBranch(nodes)
{
	var rtArr = new Array();
	for (var i = 0; i < nodes.children.length; i++) {
		var c = nodes.children[i];
		if(c.url)
		{
		  (c.url).indexOf("http")==0?rtArr.push(c.url):"";
		}else{
			rtArr = rtArr.concat(getBranch(c));
		}
	};
	return rtArr;
}
//产生[start,end]之间的随机数
function Random(start,end)
{
	start--;
   return Math.ceil(Math.random()*(end-start)+start);
}
